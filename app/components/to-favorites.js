import Ember from 'ember';

export default Ember.Component.extend({
	click() {
		var element = Ember.$(".getgif").attr("src");

		var oldItems = JSON.parse(localStorage.getItem('nam')) || [];
		
		var fid = oldItems.indexOf(element);

		if (fid === -1) {
			oldItems.push(element);
			localStorage.setItem('nam', JSON.stringify(oldItems));
			alert("Гіфка додадана, ви межете переглянути її в розділі 'Обрані'");
		} else {
			alert("ви вже добавили таку гіфку");
		}
  	}
});