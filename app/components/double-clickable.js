import Ember from 'ember';

export default Ember.Component.extend({
  click: function() {
  	Ember.$(".issue-number").removeClass("even"); 
  	this.$("img").addClass("even");
  	var link = Ember.$(".issue-number.even").attr("src");
  	localStorage.setItem('textSize', link);
  }
});