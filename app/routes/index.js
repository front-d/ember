import Ember from 'ember';

export default Ember.Route.extend({
  	model: function() {
      return Ember.$.getJSON('http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC&limit=24').then(function(json) {
	      	return json.data;
	    });
    }
});