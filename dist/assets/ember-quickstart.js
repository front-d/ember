"use strict";

/* jshint ignore:start */



/* jshint ignore:end */

define('ember-quickstart/app', ['exports', 'ember', 'ember-quickstart/resolver', 'ember-load-initializers', 'ember-quickstart/config/environment'], function (exports, _ember, _emberQuickstartResolver, _emberLoadInitializers, _emberQuickstartConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _emberQuickstartConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _emberQuickstartConfigEnvironment['default'].podModulePrefix,
    Resolver: _emberQuickstartResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _emberQuickstartConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define("ember-quickstart/components/double-clickable", ["exports", "ember"], function (exports, _ember) {
  exports["default"] = _ember["default"].Component.extend({
    click: function click() {
      _ember["default"].$(".issue-number").removeClass("even");
      this.$("img").addClass("even");
      var link = _ember["default"].$(".issue-number.even").attr("src");
      localStorage.setItem('textSize', link);
    }
  });
});
define("ember-quickstart/components/set-comp", ["exports", "ember"], function (exports, _ember) {
	exports["default"] = _ember["default"].Component.extend({
		didInsertElement: function didInsertElement() {
			_ember["default"].$(".getgif").attr("src", localStorage.getItem('textSize'));
		}
	});

	// localStorage.getItem('textSize')
});
define("ember-quickstart/components/to-favorites", ["exports", "ember"], function (exports, _ember) {
	exports["default"] = _ember["default"].Component.extend({
		click: function click() {
			var element = _ember["default"].$(".getgif").attr("src");

			var oldItems = JSON.parse(localStorage.getItem('nam')) || [];

			var fid = oldItems.indexOf(element);

			if (fid === -1) {
				oldItems.push(element);
				localStorage.setItem('nam', JSON.stringify(oldItems));
				alert("Гіфка додадана, ви межете переглянути її в розділі 'Обрані'");
			} else {
				alert("ви вже добавили таку гіфку");
			}
		}
	});
});
define('ember-quickstart/helpers/app-version', ['exports', 'ember', 'ember-quickstart/config/environment'], function (exports, _ember, _emberQuickstartConfigEnvironment) {
  exports.appVersion = appVersion;
  var version = _emberQuickstartConfigEnvironment['default'].APP.version;

  function appVersion() {
    return version;
  }

  exports['default'] = _ember['default'].Helper.helper(appVersion);
});
define('ember-quickstart/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _emberInflectorLibHelpersPluralize) {
  exports['default'] = _emberInflectorLibHelpersPluralize['default'];
});
define('ember-quickstart/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _emberInflectorLibHelpersSingularize) {
  exports['default'] = _emberInflectorLibHelpersSingularize['default'];
});
define('ember-quickstart/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'ember-quickstart/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _emberQuickstartConfigEnvironment) {
  var _config$APP = _emberQuickstartConfigEnvironment['default'].APP;
  var name = _config$APP.name;
  var version = _config$APP.version;
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(name, version)
  };
});
define('ember-quickstart/initializers/container-debug-adapter', ['exports', 'ember-resolver/container-debug-adapter'], function (exports, _emberResolverContainerDebugAdapter) {
  exports['default'] = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _emberResolverContainerDebugAdapter['default']);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('ember-quickstart/initializers/data-adapter', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `data-adapter` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'data-adapter',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('ember-quickstart/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data/-private/core'], function (exports, _emberDataSetupContainer, _emberDataPrivateCore) {

  /*
  
    This code initializes Ember-Data onto an Ember application.
  
    If an Ember.js developer defines a subclass of DS.Store on their application,
    as `App.StoreService` (or via a module system that resolves to `service:store`)
    this code will automatically instantiate it and make it available on the
    router.
  
    Additionally, after an application's controllers have been injected, they will
    each have the store made available to them.
  
    For example, imagine an Ember.js application with the following classes:
  
    App.StoreService = DS.Store.extend({
      adapter: 'custom'
    });
  
    App.PostsController = Ember.Controller.extend({
      // ...
    });
  
    When the application is initialized, `App.ApplicationStore` will automatically be
    instantiated, and the instance of `App.PostsController` will have its `store`
    property set to that instance.
  
    Note that this code will only be run if the `ember-application` package is
    loaded. If Ember Data is being used in an environment other than a
    typical application (e.g., node.js where only `ember-runtime` is available),
    this code will be ignored.
  */

  exports['default'] = {
    name: 'ember-data',
    initialize: _emberDataSetupContainer['default']
  };
});
define('ember-quickstart/initializers/export-application-global', ['exports', 'ember', 'ember-quickstart/config/environment'], function (exports, _ember, _emberQuickstartConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_emberQuickstartConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _emberQuickstartConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_emberQuickstartConfigEnvironment['default'].modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('ember-quickstart/initializers/injectStore', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `injectStore` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'injectStore',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('ember-quickstart/initializers/store', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `store` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'store',
    after: 'ember-data',
    initialize: function initialize() {}
  };
});
define('ember-quickstart/initializers/transforms', ['exports', 'ember'], function (exports, _ember) {

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `transforms` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */

  exports['default'] = {
    name: 'transforms',
    before: 'store',
    initialize: function initialize() {}
  };
});
define("ember-quickstart/instance-initializers/ember-data", ["exports", "ember-data/-private/instance-initializers/initialize-store-service"], function (exports, _emberDataPrivateInstanceInitializersInitializeStoreService) {
  exports["default"] = {
    name: "ember-data",
    initialize: _emberDataPrivateInstanceInitializersInitializeStoreService["default"]
  };
});
define('ember-quickstart/models/index', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].JSONSerializer.extend({});
});
define('ember-quickstart/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  exports['default'] = _emberResolver['default'];
});
define('ember-quickstart/router', ['exports', 'ember', 'ember-quickstart/config/environment'], function (exports, _ember, _emberQuickstartConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _emberQuickstartConfigEnvironment['default'].locationType,
    rootURL: _emberQuickstartConfigEnvironment['default'].rootURL
  });

  Router.map(function () {
    this.route('page-giff');
    this.route('favorite');
  });

  exports['default'] = Router;
});
define('ember-quickstart/routes/favorite', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({
    model: function model() {
      return JSON.parse(localStorage.getItem('nam'));
    }
  });
});
define('ember-quickstart/routes/index', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Route.extend({
        model: function model() {
            return _ember['default'].$.getJSON('http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC&limit=24').then(function (json) {
                return json.data;
            });
        }
    });
});
define('ember-quickstart/routes/page-giff', ['exports', 'ember'], function (exports, _ember) {
	var items = localStorage.getItem('textSize');
	console.log(items);
	exports['default'] = _ember['default'].Route.extend({
		model: function model() {
			return items;
		}
	});
});
define('ember-quickstart/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _emberAjaxServicesAjax) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberAjaxServicesAjax['default'];
    }
  });
});
define("ember-quickstart/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template({ "id": "snIqmbFt", "block": "{\"statements\":[[\"open-element\",\"div\",[]],[\"static-attr\",\"class\",\"container\"],[\"flush-element\"],[\"text\",\"\\n  \"],[\"open-element\",\"div\",[]],[\"static-attr\",\"class\",\"body\"],[\"flush-element\"],[\"text\",\"\\n    \"],[\"append\",[\"unknown\",[\"outlet\"]],false],[\"text\",\"\\n  \"],[\"close-element\"],[\"text\",\"\\n\"],[\"close-element\"]],\"locals\":[],\"named\":[],\"yields\":[],\"blocks\":[],\"hasPartials\":false}", "meta": { "moduleName": "ember-quickstart/templates/application.hbs" } });
});
define("ember-quickstart/templates/favorite", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template({ "id": "sXe3dwFn", "block": "{\"statements\":[[\"open-element\",\"div\",[]],[\"static-attr\",\"class\",\"menu\"],[\"flush-element\"],[\"text\",\"\\n\"],[\"block\",[\"link-to\"],[\"index\"],null,2],[\"close-element\"],[\"text\",\"\\n\\n\"],[\"block\",[\"each\"],[[\"get\",[\"model\"]]],null,1,0]],\"locals\":[],\"named\":[],\"yields\":[],\"blocks\":[{\"statements\":[[\"text\",\"Ви не обрали жодну гіфку\\n\"]],\"locals\":[]},{\"statements\":[[\"open-element\",\"img\",[]],[\"static-attr\",\"class\",\"getgif\"],[\"dynamic-attr\",\"src\",[\"concat\",[[\"get\",[\"fav\"]]]]],[\"static-attr\",\"width\",\"400\"],[\"static-attr\",\"height\",\"222\"],[\"flush-element\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[\"fav\"]},{\"statements\":[[\"text\",\"   \"],[\"open-element\",\"em\",[]],[\"flush-element\"],[\"text\",\"Повернутись на головну\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[]}],\"hasPartials\":false}", "meta": { "moduleName": "ember-quickstart/templates/favorite.hbs" } });
});
define("ember-quickstart/templates/index", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template({ "id": "lsGmI5mW", "block": "{\"statements\":[[\"open-element\",\"div\",[]],[\"static-attr\",\"class\",\"menu\"],[\"flush-element\"],[\"text\",\"\\n\"],[\"block\",[\"link-to\"],[\"favorite\"],null,4],[\"close-element\"],[\"text\",\"\\n\\n\\n\"],[\"open-element\",\"ul\",[]],[\"static-attr\",\"class\",\"list-gif\"],[\"flush-element\"],[\"text\",\"\\n\"],[\"block\",[\"each\"],[[\"get\",[\"model\"]]],null,3,0],[\"close-element\"]],\"locals\":[],\"named\":[],\"yields\":[],\"blocks\":[{\"statements\":[[\"text\",\"\\tЯкась помилка, хоча її не повинно бути\\n\"]],\"locals\":[]},{\"statements\":[[\"text\",\"    \\t\\t\"],[\"open-element\",\"img\",[]],[\"static-attr\",\"class\",\"issue-number\"],[\"dynamic-attr\",\"src\",[\"concat\",[\"https://media.giphy.com/media/\",[\"unknown\",[\"pr\",\"id\"]],\"/giphy.gif\"]]],[\"static-attr\",\"width\",\"200\"],[\"static-attr\",\"height\",\"111\"],[\"flush-element\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[]},{\"statements\":[[\"block\",[\"double-clickable\"],null,[[\"class\"],[\"som-js\"]],1]],\"locals\":[]},{\"statements\":[[\"text\",\"  \"],[\"open-element\",\"li\",[]],[\"static-attr\",\"class\",\"list-gif__item\"],[\"flush-element\"],[\"text\",\"\\n\"],[\"block\",[\"link-to\"],[\"page-giff\"],null,2],[\"text\",\"  \"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[\"pr\"]},{\"statements\":[[\"text\",\"   \"],[\"open-element\",\"em\",[]],[\"flush-element\"],[\"text\",\"Обрані\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[]}],\"hasPartials\":false}", "meta": { "moduleName": "ember-quickstart/templates/index.hbs" } });
});
define("ember-quickstart/templates/page-giff", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template({ "id": "uy9wlh6J", "block": "{\"statements\":[[\"open-element\",\"div\",[]],[\"static-attr\",\"class\",\"menu\"],[\"flush-element\"],[\"text\",\"\\n\"],[\"block\",[\"link-to\"],[\"index\"],null,3],[\"block\",[\"link-to\"],[\"favorite\"],null,2],[\"close-element\"],[\"text\",\"\\n\\n\"],[\"block\",[\"set-comp\"],null,null,1],[\"text\",\"\\n\"],[\"block\",[\"to-favorites\"],null,[[\"class\"],[\"cur-point\"]],0]],\"locals\":[],\"named\":[],\"yields\":[],\"blocks\":[{\"statements\":[[\"text\",\"    В обрані\\n\"]],\"locals\":[]},{\"statements\":[[\"open-element\",\"img\",[]],[\"static-attr\",\"class\",\"getgif\"],[\"dynamic-attr\",\"src\",[\"concat\",[\"https://media.giphy.com/media/\",[\"unknown\",[\"model\",\"items\"]],\"/giphy.gif\"]]],[\"static-attr\",\"width\",\"400\"],[\"static-attr\",\"height\",\"222\"],[\"flush-element\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[]},{\"statements\":[[\"text\",\"   \"],[\"open-element\",\"em\",[]],[\"flush-element\"],[\"text\",\"Обрані\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[]},{\"statements\":[[\"text\",\"   \"],[\"open-element\",\"em\",[]],[\"flush-element\"],[\"text\",\"Повернутись на головну\"],[\"close-element\"],[\"text\",\"\\n\"]],\"locals\":[]}],\"hasPartials\":false}", "meta": { "moduleName": "ember-quickstart/templates/page-giff.hbs" } });
});
/* jshint ignore:start */



/* jshint ignore:end */

/* jshint ignore:start */

define('ember-quickstart/config/environment', ['ember'], function(Ember) {
  var prefix = 'ember-quickstart';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

/* jshint ignore:end */

/* jshint ignore:start */

if (!runningTests) {
  require("ember-quickstart/app")["default"].create({"name":"ember-quickstart","version":"0.0.0+a25d59e7"});
}

/* jshint ignore:end */
//# sourceMappingURL=ember-quickstart.map
